      SUBROUTINE sort3(n,arr,brr,crr)

! sort the real array [arr] in ascending order, and reorder the 
! complex entries in arrays [brr] and [crr] accordingly.
 
      use my_precision
      INTEGER :: n
      REAL(kind=wp) :: arr(n)
      complex(kind=wp) :: brr(n), crr(n)
      integer, PARAMETER :: M=7,NSTACK=50
      INTEGER :: i,ir,j,jstack,k,l,istack(NSTACK)
      REAL(kind=wp) :: a,temp
      complex(kind=wp) :: b, tempb, c, tempc
      jstack=0
      l=1
      ir=n
1     if(ir-l.lt.M)then
        do 12 j=l+1,ir
          a=arr(j)
          b=brr(j)
          c=crr(j)
          do 11 i=j-1,l,-1
            if(arr(i).le.a)goto 2
            arr(i+1)=arr(i)
            brr(i+1)=brr(i)
            crr(i+1)=crr(i)
11        continue
          i=l-1
2         arr(i+1)=a
          brr(i+1)=b
          crr(i+1)=c
12      continue
        if(jstack.eq.0)return
        ir=istack(jstack)
        l=istack(jstack-1)
        jstack=jstack-2
      else
        k=(l+ir)/2
        temp=arr(k)
        arr(k)=arr(l+1)
        arr(l+1)=temp
        tempb=brr(k)
        tempc=crr(k)
        brr(k)=brr(l+1)
        crr(k)=crr(l+1)
        brr(l+1)=tempb
        crr(l+1)=tempc
        if(arr(l).gt.arr(ir))then
          temp=arr(l)
          arr(l)=arr(ir)
          arr(ir)=temp
          tempb=brr(l)
          tempc=crr(l)
          brr(l)=brr(ir)
          crr(l)=crr(ir)
          brr(ir)=tempb
          crr(ir)=tempc
        endif
        if(arr(l+1).gt.arr(ir))then
          temp=arr(l+1)
          arr(l+1)=arr(ir)
          arr(ir)=temp
          tempb=brr(l+1)
          tempc=crr(l+1)
          brr(l+1)=brr(ir)
          crr(l+1)=crr(ir)
          brr(ir)=tempb
          crr(ir)=tempc
        endif
        if(arr(l).gt.arr(l+1))then
          temp=arr(l)
          arr(l)=arr(l+1)
          arr(l+1)=temp
          tempb=brr(l)
          tempc=crr(l)
          brr(l)=brr(l+1)
          crr(l)=crr(l+1)
          brr(l+1)=tempb
          crr(l+1)=tempc
        endif
        i=l+1
        j=ir
        a=arr(l+1)
        b=brr(l+1)
        c=crr(l+1)
3       continue
          i=i+1
        if(arr(i).lt.a)goto 3
4       continue
          j=j-1
        if(arr(j).gt.a)goto 4
        if(j.lt.i)goto 5
        temp=arr(i)
        arr(i)=arr(j)
        arr(j)=temp
        tempb=brr(i)
        tempc=crr(i)
        brr(i)=brr(j)
        crr(i)=crr(j)
        brr(j)=tempb
        crr(j)=tempc
        goto 3
5       arr(l+1)=arr(j)
        arr(j)=a
        brr(l+1)=brr(j)
        crr(l+1)=crr(j)
        brr(j)=b
        crr(j)=c
        jstack=jstack+2
        if(jstack.gt.NSTACK)pause 'NSTACK too small in sort2'
        if(ir-i+1.ge.j-l)then
          istack(jstack)=ir
          istack(jstack-1)=i
          ir=j-1
        else
          istack(jstack)=j-1
          istack(jstack-1)=l
          l=i
        endif
      endif
      goto 1
      END
