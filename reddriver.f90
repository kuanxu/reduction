SUBROUTINE REDDRIVER(n, p, w, err_ord, np, pole, weight, errorbound, m)

! Purpose
! =======

! This driver routine first call subroutine dist.f90 which analyze the 
! distribution of the poles to regroup them, then call subroutine 
! squareroot.f90 to reduce the number of the poles in each subgroup and
! finally it returns with the reduced poles and weights.

! Arguments
! =========

! n           (input) integer
!             the number of poles which needs to be reduced

! p           (input) complex(kind=wp)
!             the pole position

! w           (input) complex(kind=wp)
!             the corresponding weights/residues to poles

! err_ord     (input) real(kind=wp)
!             the exponent of error bound for the reduced model. Then
!             the input error bound is 10.0_wp**err_ord.

! np          (output) integer
!             the number of the reduced poles

! pole        (output) complex(kind=wp)
!             the first np entries are the position of the poles after reduction

! weight      (output) complex(kind=wp)
!             the first np entries record the corresponding weights to the reduced poles

! errorbound  (output) real(kind=wp)
!             the error bound given by the squareroot method. It should be
!             smaller than the required error bound 10.0_wp**err_oder 

! m           (output) integer
!             the number of the subgroups given by dist.f90. It is as 
!             well the times the suboutines squareroot.f90 is called.


 
! =======================THE START OF THE DRIVER========================

! declaration of the variables and initialization
use my_precision
implicit none
integer, intent(in) :: n
integer, intent(out) :: np, m
integer :: i, gr, in, k
complex(kind=wp), intent(in) :: p(n), w(n)
complex(kind=wp), intent(out) :: pole(n), weight(n)
complex(kind=wp), allocatable :: pt(:), wt(:)

!real(kind=wp) :: ap(n)
real(kind=wp) :: err_m, bnd, errorbound
real(kind=wp), intent(in) :: err_ord
real(kind=wp) :: err

pole = (0.0,0.0)
weight = (0.0,0.0)

err = 10.0_wp**(err_ord)

in = 1
errorbound = 0
np = 0

! analyze the distribution of the poles and regroup them. each subgroup
! will be treated separately

call dist(n, p, err_ord, m, gr)

err_m = err/dble(m)

! call subroutine squareroot.f90 to reduce each subgroup

do i = 1,m
   if (i/=m) then
      allocate(wt(gr))
      allocate(pt(gr))
      call squareroot(w(in:in+gr-1), p(in:in+gr-1), gr, err_m, wt, pt, k, bnd)

      in = in+gr

      pole(np+1:np+k) = pt(1:k)
      weight(np+1:np+k) = wt(1:k)
      errorbound = errorbound+bnd

      np = np+k
      deallocate(wt)
      deallocate(pt)
   else
      allocate(wt(n-in+1))
      allocate(pt(n-in+1))
      call squareroot(w(in:n), p(in:n), n-in+1, err_m, wt, pt, k, bnd)
      
      pole(np+1:np+k) = pt(1:k)
      weight(np+1:np+k) = wt(1:k)
      errorbound = errorbound+bnd

      np = np+k
      deallocate(wt)
      deallocate(pt)
   endif
enddo

! =======================THE END OF THE DRIVER==========================
END SUBROUTINE REDDRIVER
