SUBROUTINE SQUAREROOT(w, s, n, err, weight, pole, k, bnd)

! Purpose
! =======

! This subroutine reduces the original rational approximation
! with [n] poles [s] and the corresponding weights [w] to the one
! with [k] poles at [pole] weighting [weight]. The required error
! [err] is to be satisfied and the theoretical error bound [bnd] 
! obtained by computing the Hankel singular values of the 
! associated model system is a more accurate estimate of the 
! error of the reduced rational approximation. [bnd] is given as 
! one of the outputs and supposed to be smaller than [err].

! Arguments
! =========

! w           (input) complex(kind=wp)
!             the weights corresponding to the poles in the original 
!             rational approximation
 
! s           (input) complex(kind=wp)
!             the pole positions in the original rational 
!             approximation

! n           (input) integer
!             the number of the poles in the original rational
!             approximation

! err         (input) real(kind=wp)
!             the required error bound for the reduction

! weight      (output) complex(kind=wp)
!             the weights corresponding to the poles in the reduced
!             rational approximation

! pole        (output) complex(kind=wp)
!             the pole positions in the reduced rational approximation

! k           (output) integer(kind=wp)
!             the number of the poles in the reduced rational 
!             approximation

! bnd         (output) real(kind=wp)
!             the error bound obtained by computing the Hankel 
!             singular values of the associated model system

! Algorithm
! =========

! Step 1:

! Form the state-space realization of the transfer function
  
!               G(z) = C*((Iz-A)**(-1))*B
! by letting

!             diag(A) = s       and       B = C = sqrt(w).

! Step 2:

! Compute the solution P and Q of the Lyapunov equations

!              P*(A**H)+A*P+B*(B**H)=0
!              Q*A+(A**H)*Q+(C**H)*C=0

! Instead of computing the solution directly, we compute the Cholesky 
! factorization of the solutions to the Lyapunov equations, i.e.

!            Lr*(Lr**H)=P   and    Lo*(Lo**H)=Q

! Step 3:

! Compute the singular value decomposition of (Lo**H)*Lr, i.e.

!              U*S*(V**H)=(Lo**H)*Lr  

! The diagonal entries of S, denoted by s1, s2,..., sn is termed 
! Hankel singular values of the original realization and 
! off-diagonal entries of S are all zeros. Then we compute 
! the bound for the reduced rational approximation Gr(z) as

!                                       n
!            ||G(z)-Gr(z)||    <= 2*   sum   s
!                          inf        i=k+1   i

! as we reduce the system to order of k.

! Step 4:

! Compute the left and right eigenvectors associated with eigenvalues 
! s1**2, s2**2,...,sk**2 (1<=k<=n) of P*Q in a truncated fashion.

!                   -1/2
! We denote c  = (s )    for i = 1,...,k. 
!            i     i
! Then
!                 / c1        \
!                 |   c2      |
!                 |     .     |
!       SL = Lo*U*|       .   |
!                 |         . |
!                 |           |
!                 \     0     /

!                 / c1        \
!                 |   c2      |
!                 |     .     |
!  and  SR = Lr*V*|       .   |
!                 |         . |
!                 |           |
!                 \     0     /

! Step 5:

! We obtain the reduced realization as
 
! Ahat = (SL**H)*A*SR
! Bhat = (SL**H)*B
! Chat = C*SR

! Step 6:

! We compute the reduced poles by first finding the spectral 
! decomposition of A_hat as

!            Ahat = T*D*(T**(-1)),

! and then extracting the diagonal entries of D, i.e.

!            pole = diag(D).

! For the weights corresponding to the reduced poles, we 
! need to compute 

!          Ct = Chat*T   and   Bt = (T**(-1))*Bhat,

! where Ct and Bt are both k-dimensional arrays.

! Finally, 
!          weight(i) = Ct(i)*Bt(i)  i = 1,...,k



! References
! ==========

! [1]. M. G. Safonov and R. Y. Chiang, "A Schur method for 
!    Balanced-Truncation model reduction", IEEE Trans. 
!    Automat. Control 34 (1989), no. 34, 729-733.

! [2]. M. S. Tombs and I. Postlethwaite, "Truncated balanced 
!    realization of a stable non-minimal state-space system",
!    Int. J. Control 46 (1987), no. 4, 1319-1330.

! [3]. J. R. Li, "Low order approximation of the spherical 
!    nonreflecting boundary kernel for the wave equation",
!    Linear Algebra and its Applications 415 (2006), 455-468.


! =================THE START OF THE PROGRAM=====================

! the declaration of the variables and initialization

use my_precision
implicit none
integer, intent(in) :: n
complex(kind=wp), intent(in) :: w(n), s(n)
real(kind=wp), intent(in) :: err
integer, intent(out) :: k
complex(kind=wp), intent(out) :: weight(n), pole(n)
real(kind=wp), intent(out) :: bnd

integer, parameter :: lwork1 = 40000
integer, parameter :: lwork2 = 60000
integer, parameter :: lwork3 = 40000
integer, parameter :: lwork4 = 40000

complex(kind=wp) :: A(n), B(n), C(n), Rr(n,n), Ro(n,n), Lr(n,n), Lo(n,n), P(n,n), Q(n,n), PQ(n,n), E(n)
complex(kind=wp) :: VL, VR, LoLr(n,n), U(n,n), VT(n,n), V(n,n), AA(n,n)
complex(kind=wp) :: work1(lwork1), work2(lwork2), work3(lwork3), work4(lwork4)
!real(kind=wp) :: P(n,n), Q(n,n), PQ(n,n)
real(kind=wp) :: bound(n), rwork1(2*n), rwork2(5*n), rwork3(2*n), hsv(n), SS(n), SSS(n)
integer :: info1, info2, info3, info4, i, index
integer, allocatable :: IPIV(:)
complex(kind=wp), allocatable :: H(:,:), Ahat(:,:), Bhat(:), Chat(:), VLhat(:,:)
complex(kind=wp), allocatable :: VRhat(:,:), V_inv(:,:), IVIP(:), Ct(:), Bt(:), SL(:,:), SR(:,:)

AA = (0.0,0.0)

! Step 1: assign the values of A, B, and C

A = s
B = sqrt(w)
C = B

! Step 2: solve the Lyapunov equations for the Cholesky 
!         factorizations of the solutions


call lyapchol(A,B,n,Rr)

Lr = transpose(conjg(Rr))

call lyapchol(conjg(A),conjg(C),n,Ro)

Lo = transpose(conjg(Ro))


! Step 3: compute the Hankel singular values, and then the
!         bounds for each order of the reductions


LoLr = matmul(transpose(conjg(Lo)),Lr)

call ZGESVD('A', 'A', n, n, LoLr, n, SS, U, n, VT, n, work2, lwork2, rwork2, info2)

V = transpose(conjg(VT))

hsv = abs(SS)

call sort(n, hsv)

do i = 1,n
   bound(i) = 2.0_wp*sum(hsv(1:i))
enddo

! Determine if the reduction can be achieved
 
if (bound(1)>=err) then

k = n
weight = w
pole = s
bnd = 0

else

index = count(bound <= err)
k = n-index

bnd = bound(index)

allocate(Ahat(k,k))
allocate(Bhat(k))
allocate(Chat(k))
allocate(VLhat(k,k))
allocate(VRhat(k,k))
allocate(V_inv(k,k))
allocate(IPIV(k))
allocate(Bt(k))
allocate(Ct(k))
allocate(SL(n,k))
allocate(SR(n,k))
allocate(H(n,k))

H = 0.0_wp

SSS = SS**(-0.5_wp)

do i = 1,k
   H(i,i) = SSS(i)
enddo

! Step 4: compute the left and right eigenvectors SL 
!         and SR respectively


SL = matmul(matmul(Lo,U),H)
SR = matmul(matmul(Lr,V),H)

do i = 1,n
   AA(i,i) = A(i)
enddo

! Step 5: compute the reduced realization Ahat, Bhat,
!         and Chat


Ahat = matmul(matmul(transpose(conjg(SL)),AA),SR)
Bhat = matmul(transpose(conjg(SL)),B)
Chat = matmul(C,SR)

! Step 6: compute the reduced poles and weights


call ZGEEV('N', 'V', k, Ahat, k, pole(1:k), VLhat, k, VRhat, k, work3, lwork3, rwork3, info3)

Ct = matmul(Chat,VRhat)

V_inv = VRhat

do i = 1,k
   IPIV(i) = k-i+1
enddo

call ZGESV(k, 1, VRhat, k, IPIV, Bhat, k, info4)

Bt = Bhat

do i = 1,k
   weight(i) = Ct(i)*Bt(i)
enddo

endif

! ===================THE END OF THE PROGRAM=====================

END SUBROUTINE SQUAREROOT
