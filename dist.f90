SUBROUTINE DIST(n, p, err_ord, m, gr)

! Purpose
! =======

! This subroutine analyzes the distribution of the poles. 

! This subroutine regroup poles and their corresponding 
! weights according to the farness of the poles from the
! origin. The poles in each subgroup span over the order
! range [span] = 16.0_wp-(-[err_ord]). By doing this, we can 
! make sure that the actual error of the reduced rational 
! approximation will be smaller than the error bound 
! obtained by computing the Hankel singular values of the
! associated control system.

! The poles in each of the subgroups will be treated 
! individually in the reduction driver routine reddriver.f90.

! Arguments
! =========

! n        (input) integer
!          the number of the poles in the original 
!          rational approximation

! p        (input) complex(kind=wp)
!          the pole postions

! err_ord  (input) real(kind=wp)
!          the required error bound inputed in the reddriver.f90

! m        (output) integer
!          the number of the subgroups

! gr       (output) integer
!          the number of the poles in each subgroup


!=================THE START OF THE PROGRAM========================

! the declaration of the variables and the initialization
use my_precision
implicit none
integer, intent(in) :: n
integer, intent(out) :: m, gr
complex(kind=wp), intent(in) :: p(n)
real(kind=wp), intent(in) :: err_ord
real(kind=wp) :: abs_p(n), l_abs_p(n), intgr(n), ns, span

! compute the number of the subgroups and the number of the poles
! in each subgroup averagely.

abs_p = abs(p)
l_abs_p = log10(abs_p)
intgr = floor(l_abs_p)
ns = maxval(intgr)-minval(intgr)+1.0_wp
span = 16.0_wp+err_ord
!span = 4.0_wp
m = anint(ns/span)
gr = ceiling(dble(n)/dble(m))

! ===================THE END OF THE PROGRAM========================
END SUBROUTINE DIST
