SUBROUTINE LYAPCHOL(A,B,n,U)

! Purpose
! =======

! This subroutine computes a Cholesky factorization 

!                 X = (R**H)*R 

! of the solution X to a special case of the Lyapunov 
! matrix equation

!           P*X + X*(P**H) + B*(B**H) = 0

! where P is a diagonal matrix whose off-diagonal 
! entries are all zeros and B is an [n] by 1 column
! vector.


! Arguments
! =========

! A           (input) complex(kind=wp) dimension [n]
!             A = diag(P)  

! B           (input) complex(kind=wp) dimension [n]
!             B is an [n] by 1 column vector

! n           (input) integer
!             The dimension of A

! U           (output) complex(kind=wp) dimension ([n],[n])
!             The Cholesky factorization of the solution

!             P*(R**H)*R + (R**H)*R*(P**H) + B*(B**H) = 0,

!             where diag(P) = A and B is of [n] by 1.                                       


! Caution
! =======

! Unlike a standard Lyapunov-Cholesky solver based on the 
! implementation of Hammarling's algorithm, this subroutine
! deals with a special case of Lyapunov equation in which 
! P has all of its off-diagonal entries zero and B is only
! an [n] by 1 column vector. 

! These special features reduce Hammarling's algorithm 
! to some extent, according to [2].

! Algorithm and comments
! ======================

! The algorithm implemented in this subroutine is a reduced 
! variant of the case discussed in Section 7 of [2] due to 
! Hammarling.

! Other existing libraries, such as SLICOT/NICONET, do not
! provide the subroutines for complex variables. Hence, 
! routines like SB03OD and SG03BD are not useful or have to
! be modified accordingly before use for our problem.

! References
! ==========

! [1] Bartels, R.H. and G.W. Stewart, "Solution of the Matrix 
!     Equation AX + XB = C," Comm. of the ACM, Vol. 15, No. 9,
!     1972. 

! [2] Hammarling, S.J., "Numerical solution of the stable, 
!     non-negative definite Lyapunov equation," IMA J. Num. Anal.,
!     Vol. 2, pp. 303-325, 1982. 

! [3] Penzl, T., "Numerical solution of generalized Lyapunov 
!     equations," Advances in Comp. Math., Vol. 8, pp. 33-48, 1998.


! =================THE START OF THE PROGRAM===================

! the declaration of variables and the initialization

USE my_precision
IMPLICIT NONE

integer, intent(in) :: n
complex(kind=wp), intent(in) :: B(n)
complex(kind=wp), intent(out) :: U(n,n)
complex(kind=wp) :: A(n), AC(n)
complex(kind=wp) :: alpha, tau(n)
complex(kind=wp), allocatable :: uu(:), F(:,:), R(:,:), y(:)
integer :: i, j, k, info
integer, parameter :: lwork = 40000
complex(kind=wp) work(lwork)

U = 0
AC = conjg(A)

allocate(R(n,n))
R = 0
R(1,:) = conjg(B)


! The recursive algorithm for computing R(i,i:n) and R(i+1:n,i)

do i = 1,n-1
   allocate(uu(n-i))
   allocate(F(n-i+1,n-i))
   allocate(y(n-i))
   
   U(i,i) = abs(R(1,1))/sqrt(-2*real(AC(i)))
   alpha = R(1,1)/U(i,i)
   uu = (0.0,0.0)
   do j = i+1,n
      uu(j-i) = -(alpha*conjg(R(1,j-i+1)))/(conjg(AC(j))+AC(i))
   enddo
   
   U(i,i+1:n) = conjg(uu)
   y = conjg(R(1,2:n-i+1))-conjg(alpha)*uu
   F(1:n-i,:) = R(2:n-i+1,2:n-i+1)

   deallocate(R)
   allocate(R(n-i,n-i))
   R = (0.0,0.0)

   F(n-i+1,:) = conjg(y)
   call ZGEQRF(n-i+1, n-i, F, n-i+1, tau, work, lwork, info)
   
   do k = 1,n-i
      R(k,k:n-i) = F(k,k:n-i) 
   enddo

   deallocate(uu)
   deallocate(F)
   deallocate(y)
enddo

U(n,n) = abs(R(1,1))/sqrt(-2*real(AC(n)))

! =================  THE END OF THE PROGRAM===================

END SUBROUTINE LYAPCHOL


